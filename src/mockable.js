const API_URL = 'http://demo2875349.mockable.io';
const Login_URL = 'http://demo3026586.mockable.io';

export const fetchProducts = () =>
  fetch(`${API_URL}/products`).then((res) => {
    if (res.status === 401) {
      console.log('error')
      return { ok: false, products: null };
    }
    return res.json().then(products => ({ ok: true, products }));
  });

export const fetchBrands = () =>
  fetch(`${API_URL}/brands`).then((res) => {
    if (res.status === 401) {
      console.log('error')
      return { ok: false, brands: null };
    }
    return res.json().then(brands => ({ ok: true, brands }));
  });

export const fetchLogin = (body) => {
  const options = {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
    },
  };
  return fetch(`${Login_URL}/user`, options).then((res) => {
    if (res.status === 200) {
      return res.json().then(message => ({ ok: true, message }));
    }
    return { ok: false, message: null };
  });
};

