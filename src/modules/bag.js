const initialState = {
    bagProducts: [],
};

const INCREMENT = 'INCREMENT';
const DECREMENT = 'DECREMENT';
const REMOVE = 'REMOVE';
const BUY = 'BUY';
const ADD_TO_CART = 'ADD_TO_CART';

export const increment = (data) => ({
    type: INCREMENT,
    data,
});

export const decrement = (data) => ({
    type: DECREMENT,
    data,
});

export const remove = (data) => ({
    type: REMOVE,
    data,
});

export const buy = () => ({
    type: BUY,
});

export const doAddToCart = (product) => {
    return {
        type: ADD_TO_CART,
        product,
    }

}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART: {
            const { product } = action;
            const { id: newId, count: newCount } = { ...product }
            return {
                ...state,
                bagProducts: state.bagProducts.every(el =>
                    el.id !== newId) ? (state.bagProducts.concat({ ...product })) : (state.bagProducts.map(el => 
                        el.id !== newId ? el : ({ ...el, count: el.count + newCount }))),
            }
        }
        case INCREMENT: {
            const { data } = action;
            return {
                ...state,
                bagProducts: state.bagProducts.map(el => el.id === data.id ? ({ ...el, count: data.count + 1 }) : el)
            };
        }
        case DECREMENT: {
            const { data } = action;
            return {
                ...state,
                bagProducts: state.bagProducts.map(el => el.id === data.id ? ({ ...el, count: data.count - 1 }) : el)
            };
        }
        case REMOVE: {
            const { data } = action;
            return {
                ...state,
                bagProducts: state.bagProducts.filter(el => el.id !== data.id)
            };
        }
        case BUY: {
            return {
                ...state,
                bagProducts: [],
            };
        }
        default:
            return state;
    }
}

