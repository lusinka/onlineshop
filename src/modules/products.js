import {fetchProducts, fetchBrands} from '../mockable';

const FETCH_PRODUCTS = 'products/FETCH_PRODUCT';
const FETCH_SUCCESS = 'products/FETCH_SUCCESS';
const FETCH_FAILED = 'products/FETCH_FAILED';


const initialState = {
    data:[],
    brands:[],
    isLoading:false,
    error:'',
}

export const doFetchProducts = ()=>({
    type:FETCH_PRODUCTS,
});

export const doFetchSuccess = (products,brands)=>({
    type:FETCH_SUCCESS,
    products,
    brands
});

export const doFetchFailed = (error)=>({
    type:FETCH_FAILED,
    error,
});

export const reducer = (state=initialState, action)=>{
    switch (action.type){
        case FETCH_PRODUCTS:{
            return{
                ...state,
                isLoading:true,
            };
        }

        case FETCH_SUCCESS:{
            return {
                ...state,
                isLoading:false,
                data: action.products,
                brands: action.brands,
            };
        }

        case FETCH_FAILED:{
            return {
                ...state,
                isLoading:false,
                error:action.error,
            };
        }

        default: return state;
    }
}

export const fetchAllProducts = ()=>(dispatch)=>{
    dispatch(doFetchProducts());

    fetchProducts().then(({ok,products})=>{
        if(!ok){
            dispatch(doFetchFailed(`Cannot fetch product list \\ API Error `));
            return
        }

        fetchBrands().then(({ok,brands})=>{
            if(!ok){
                dispatch(doFetchFailed(`Cannot fetch product list \\ API Error `));
                return
            }

        dispatch(doFetchSuccess(products,brands))})

    }).catch(err=>dispatch(doFetchFailed(err.toString())));

}
