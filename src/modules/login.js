const initialState = {
    open: false,
    login: false,
};

const CLOSEDIALOG = 'CLOSEDIALOG';
const OPENDIALOG = 'OPENDIALOG';

export const doClose = () => ({
    type: CLOSEDIALOG,
});


export const doOpen = () => ({
    type: OPENDIALOG,
});

export const reducer = (state=initialState, action)=>{
    switch (action.type){
        case CLOSEDIALOG:{
            return{
                ...state,
                open: false,
            };
        }
        case OPENDIALOG:{
            return{
                ...state,
                open: true,
            };
        }
        default: return state;
    }
}

export const doLogout = () => {
  sessionStorage.removeItem('user');
  window.location.href = '/';
};
