const CHANGE_FILTER = 'filterProducts/CHANGE_FILTER';

const initialState = {
    data:[],
    firstFilter:[],
    filterState:{
        slider:false,
        openProduct:false,
        openPrice:false,
        openBrand:false,
        Best:false,
        New:false,
        Sale:false,
        minPrice:0+'$',
        maxPrice:2500+'$',
        HP:false,
        ASUS:false,
        LG:false,
        MSI:false,
        CyberpowerPC:false,
        TCL:false,
        'Holy Stone':false,
        Syma:false,
        Apple:false,},
    
}

export const doFilter = (allProducts,filter)=>{
    let data = [];
    let dataModel=[];
    let firstFilter=[];
    let dataPrice = [];

    dataPrice=allProducts.filter(product=>(parseInt(product.price,10)>=parseInt(filter.minPrice,10) && parseInt(product.price,10)<=parseInt(filter.maxPrice,10) ));
    
    if(filter['Best']){
        data=data.concat(dataPrice.filter(product=>product.bestSeller))
    };

    if(filter['New']){
        data=data.concat(dataPrice.filter(product=>product.new))
    };

    if(filter['Sale']){
        data=data.concat(dataPrice.filter(product=>product.sale))
    };

    if(data.length===0){
        data=dataPrice;
    } else{
        data=data.filter((product,index,data)=>data.indexOf(product)===index);
    }

    firstFilter = data.map(x=>x);

    if(filter['HP']){
        dataModel=dataModel.concat(data.filter(product=>product.brand.name==='HP'));
    }

    if(filter['ASUS']){
        dataModel=dataModel.concat(data.filter(product=>product.brand.name==="ASUS"));
    }

    if(filter['LG']){
        dataModel=dataModel.concat(data.filter(product=>product.brand.name==="LG"));
    }

    if(filter['MSI']){
        dataModel=dataModel.concat(data.filter(product=>product.brand.name==="MSI"));
    }

    if(filter['CyberpowerPC']){
        dataModel=dataModel.concat(data.filter(product=>product.brand.name==="CyberpowerPC"));
    }
    
    if(filter['TCL']){
        dataModel=dataModel.concat(data.filter(product=>product.brand.name==="TCL"));
    }

    if(filter['Holy Stone']){
        dataModel=dataModel.concat(data.filter(product=>product.brand.name==="Holy Stone"));
    }
        
    if(filter['Syma']){
        dataModel=dataModel.concat(data.filter(product=>product.brand.name==="Syma"));
    }
    
    if(filter['Apple']){
        dataModel=dataModel.concat(data.filter(product=>product.brand.name==="Apple"));
    }

    if(dataModel.length>0){
        data=dataModel;
    } 
    return {
        type:CHANGE_FILTER,
        data,
        firstFilter,
        filterState:filter,
    }
}

export const reducer = (state=initialState, action)=>{
    switch (action.type){
        case CHANGE_FILTER:{
            return {
                data:action.data,
                firstFilter:action.firstFilter,
                filterState:action.filterState,
            }
        }
        default: return state;
    }
    
}

