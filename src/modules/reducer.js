import { combineReducers } from 'redux';
import {reducer as products} from './products';
import {reducer as bag} from './bag';
import { reducer as feedback } from './feedback';
import { reducer as form } from 'redux-form';
import {reducer as header} from './header';
import {reducer as filteredProducts} from './filteredProducts';
import {reducer as login} from './login';

export default combineReducers({
    products,  
    bag,
    header,
    feedback,
    form,
    filteredProducts,
    login,
});
