const initialState = {
    feedback: [
        {
            message: {
                date: '2018-03-04', name: 'David', email: 'David95@gmail.com', notes: 'I would just like to say thank you for the great products and excellent serviceI would like to thank you. It is the second time i made business with you and i can say that i am fully satisfiedI would just like to say thank you for the great products and excellent service.', id: 0,
            },
        },
        {
            message: {
                      date: '2018-03-24', name: 'Artur', email: 'Artur_@gmail.com', notes:'I would just like to say thank you for the great products and excellent service.', id: 1,
            },
        },
        {
            message: {
                      date: '2018-03-30', name: 'Daniel', email: 'Daniel_info@gmail.com', notes:'I am Daniel Rodriguez from Spain. I want to thank you for your beauty and funny products.', id: 2,
            },
        },
        {
            message: {
                      date: '2018-04-02', name: 'Lusy', email: 'Lusy90_info@gmail.com', notes:'Very impressed. Thank you and keep up the good work.', id: 3,
            },
        },
        {
            message: {
                      date: '2018-04-20', name: 'Ann', email: 'ann_@gmail.com', notes:'I would just like to say thank you for the great products and excellent serviceI would like to thank you. It is the second time i made business with you and i can say that i am fully satisfiedI would just like to say thank you for the great products and excellent service.', id: 4,
            },
        },
        {
            message: {
                      date: '2018-04-25', name: 'Bob', email: 'bob.mic_@gmail.com', notes:'I would just like to say thank you for the great products and excellent service.', id: 5,
            },
        },
        {
            message: {
                      date: '2018-05-03', name: 'Daniel', email: 'Daniel1856_@gmail.com', notes:'I am Daniel Rodriguez from Spain. I want to thank you for your beauty and funny products.', id: 6,
            },
        },
        {
            message: {
                      date: '2018-05-03', name: 'Liza', email: 'liza1856_@gmail.com', notes:'Very impressed. Thank you and keep up the good work.', id: 7,
            },
        },
    ],
    nextId: 8,
};

const SENDFEEDBACK = 'SENDFEEDBACK';

export const sendFeedback = (message) => ({
    type: SENDFEEDBACK,
    message,
});

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SENDFEEDBACK: {
            const { message } = action;
            return {
                ...state,
                feedback: [...state.feedback, { message: message, id: state.nextId}],
                nextId: state.nextId + 1,
            };
        }
        default:
            return state;
    }
}