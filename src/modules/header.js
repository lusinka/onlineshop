const CHANGE_TAB = 'header/CHANGE_TAB';

const initialState = {
    tabValue: parseInt(localStorage.getItem('tab'), 10) || 0,
}

export const doChangeTab = (value) => {

    localStorage.setItem('tab', value)
    return {
        type: CHANGE_TAB,
        value
    }
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_TAB: {
            return {
                tabValue: action.value,
            }
        }
        default: return state;
    }

}

