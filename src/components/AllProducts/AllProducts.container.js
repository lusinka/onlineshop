import { connect } from 'react-redux';
import AllProducts from './AllProducts.jsx';
import { fetchAllProducts } from '../../modules/products';
import { getProducts, getError, getIsLoading, getAllFilteredProducts } from '../../selectors/products';

const mapStateToProps = state => ({
    products: getProducts(state),
    error: getError(state),
    isLoading: getIsLoading(state),
    filteredProducts: getAllFilteredProducts(state)
});

const mapDispatchToProps = {
    fetchAllProducts,
}

export default connect(mapStateToProps, mapDispatchToProps)(AllProducts);