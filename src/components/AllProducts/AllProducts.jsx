import React, { Component, } from 'react';
import Product from '../Product/Product';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import styles from './styles';

class AllProducts extends Component {

    render() {
        const { filteredProducts, error, isLoading } = this.props;
        const { classes } = this.props;

        return isLoading ? (
            <span>Loading...</span>
        ) : (
                error ? (
                    <span>{error}</span>
                ) : (
                        <div className={this.props.classes.root}>
                            <div className={classes.productContainer} >
                                {filteredProducts.length ? filteredProducts.map(product =>
                                    <Product key={product.id} product={product} error={error} />
                                ) : (<h1>0 Results !!! :(</h1>)}
                            </div>
                        </div>
                    ))
    }
}

AllProducts.propTypes = {
    classes: PropTypes.object.isRequired,
    error: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
    filteredProducts: PropTypes.array.isRequired,
};

export default withStyles(styles)(AllProducts);
