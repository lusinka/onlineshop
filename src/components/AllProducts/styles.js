const styles = theme => ({

    root: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        'margin-top': '120px',
        padding: '0',
        'font-family': 'sans-serif',
    },
    productContainer: {
        display: 'flex',
        width: '100%',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        margin: '80px 20px 80px',
    },
});

export default styles;