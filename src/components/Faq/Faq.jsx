import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import styles from './styles';


class Faq extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true,
        };
    }

    handleClick = () => {
        this.setState({ open: false });
    }

    handleBack = () => {
        this.setState({ open: true });
    }

    render() {
        const { classes } = this.props;
        const { open } = this.state;

        return (
            <div>
                {open && (<div className={classes.root}>
                    <div className={classes.title}>
                        <h2> Top FAQs</h2>
                    </div>
                    <ul className={classes.ul}>
                        <li>How can I save when shopping online?
                            <p className={classes.text}>One of the benefits of shopping online
                                is that you are able to find products
                                at lower prices due to the economics of e-commerce.
                            </p>
                        </li>
                        <li> Are the brands authentic?
                            <p className={classes.text}>On TechShop, we ensure that our brands are authentic
                                and sourced from trust-worthy suppliers.
                            </p>
                        </li>
                        <li> Is it safe to shop online?
                            <p className={classes.text}>This is an important question and our priority
                                is to ensure the privacy of our customers and safety of payment.
                                we highly recommend you have a strong password for your shopping website.
                            </p>
                        </li>
                    </ul>
                    <div className={classes.help}>
                        <p className={classes.need}>Need help?</p>
                        <button className={classes.button} onClick={this.handleClick}>CONTACT CUSTOMER CARE</button>
                    </div>
                </div>)}
                {open || (
                    <div className={classes.root1}>
                        <p>Need help? Call us at <span className={classes.contact}>+374 55 522 320</span></p>
                        <p className={classes.contact}> OR</p>
                        <p>Email us at <span className={classes.contact}>TechShop@gmail.com</span></p>
                        <button className={classes.button} onClick={this.handleBack}>back</button>
                    </div>
                )}
            </div>

        );
    }
};

Faq.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Faq);