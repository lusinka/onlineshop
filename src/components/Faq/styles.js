const styles = theme => ({

    root: {
        width: '70%',
        height: "800px",
        margin: '190px auto 20px',
        display: 'flex',
        flexDirection: 'column',
        background: "white",
        fontSize: "25px",
        color: "#1c1d1e",

    },
    title: {
        background: "#222426",
        color: "white",
        paddingLeft: 20,
    },
    text: {
        color: "#898989",
        fontSize: "22px"
    },
    help: {
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        marginTop: "90px",
        paddingBottom: "30px"
    },
    need: {
        color: "#1c1d1e",
    },
    button: {
        backgroundColor: '#ffb74d',
        padding: "20px 30px",
        fontWeight: 'bold',
        '&:hover': {
            backgroundColor: '#1b1f26',
            color: 'white',
        },
        fontSize: "15px",
        borderRadius: "5px",
        boxShadow: "0 8px 16px 0 rgba(0,0,0,0.1), 0 6px 20px 0 rgba(0,0,0,0.19)",
    },
    root1: {
        width: '60%',
        height: "650px",
        margin: '190px auto 20px',
        display: 'flex',
        flexDirection: 'column',
        background: "#fcfaf9",
        fontSize: "18px",
        color: "#aeb0b2",
        alignItems: "center",
        justifyContent: 'center'
    },
    contact: {
        fontSize: "25px",
        color: "#2a2c2d"
    },

});

export default styles;
