import { connect } from 'react-redux';
import { doLogout } from '../../modules/login';
import Logout from './Logout';

const mapDispatchToProps = {
    doLogout,
}

export default connect(null, mapDispatchToProps)(Logout);
