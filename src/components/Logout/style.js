const styles = theme => ({

    logout: {
        cursor: 'pointer',
        padding: '0 10px 0',
    },
});

export default styles;