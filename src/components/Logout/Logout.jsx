import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import styles from './style';

class Logout extends React.Component {

    render() {
        const { classes, doLogout } = this.props;

        return (
            <span className={classes.logout} style={{ fontSize: 16, }} onClick={doLogout}>LOG OUT</span>
        );
    };
};

Logout.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Logout);
