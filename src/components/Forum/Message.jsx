import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import styles from './messageStyle';

const Message = (props) => {

    const { classes, message } = props;

    return (
        <div className={`wow slideInDown ${classes.messageContainer}`}>
            <div className={classes.info}>
                <div>
                    {message.message.name}
                </div>
                <div>
                    {message.message.date}
                </div>
            </div>
            <div className={classes.notes}>
                {message.message.notes}
            </div>
            <div>
                {message.message.email}
            </div>
        </div>
    );
}

Message.propTypes = {
    classes: PropTypes.object.isRequired,
    message: PropTypes.object.isRequired,
};

export default withStyles(styles)(Message);


