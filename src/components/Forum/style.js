const styles = {

    container: {
        width: '80%',
        margin: '190px auto 40px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
}

export default styles;