import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import WOW from 'wowjs';
import Message from './Message';
import styles from './style';

class Forum extends React.Component {

    componentDidMount() {
        const wow = new WOW.WOW();
        wow.init();
    };

    render() {
        const { classes, feedback } = this.props;

        return (
            <div className={classes.container}>
                {feedback.map(message =>
                    <Message key={message.message.id} message={message} />
                )}
            </div>
        );
    };
}

Forum.propTypes = {
    classes: PropTypes.object.isRequired,
    feedback: PropTypes.array.isRequired,
};

export default withStyles(styles)(Forum);