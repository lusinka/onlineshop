import { connect } from 'react-redux';
import { getFeedback } from '../../selectors/feedback';
import Forum from './Forum';

const mapStateToProps = state => ({
    feedback: getFeedback(state),
});

export default connect(mapStateToProps)(Forum);