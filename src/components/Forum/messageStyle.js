const styles = {

    messageContainer: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        height: 'auto',
        marginTop: 20,
        padding: 10,
        fontFamily: 'Montserrat, sans-serif',
        fontSize: '22px',
        minHeight: 90,
        textAlign: 'left',
        background: '#fff',
        borderRadius: 10,
        boxShadow: '0px 2px 5px 0px rgba(0,0,0,0.16), 0px 2px 5px 0px rgba(0,0,0,0.23)',
    },
    info: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20,
    },
    notes: {
        marginBottom: 20,
    },
}

export default styles;