import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import Typography from 'material-ui/Typography';
import { Link } from 'react-router-dom';
import ShoppingCart from '@material-ui/icons/ShoppingCart';
import Drawer from 'material-ui/Drawer';
import Cart from '../Cart/Cart.container';
import Login from '../Login/Login.container';
import Logout from '../Logout/Logout.container';
import './Header.css';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = theme => ({
  root: {
    position: 'fixed',
    'z-index': 1,
    width: '100%',
    margin: 0,
    padding: 0,
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  tabRoot: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabsRoot: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});

class Header extends React.Component {
  state = {
    right: false,
    anchorEl: null,
  };

  toggleDrawer = (side, open) => {
    this.setState({
      [side]: open,
    });
  };

  handleChange = (event, value) => {
    this.props.changeTabValue(value);
    if (value === 6) {
      this.toggleDrawer('right', true)
    }
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleCount = () => {
    const { bag } = this.props;
    let sum = 0;
    bag.forEach(bagItem => sum = sum + bagItem.count);
    return sum;
  };

  render() {
    const { tabValue, classes, } = this.props;
    const { right, anchorEl, } = this.state;
    const open = Boolean(anchorEl);
    const user = JSON.parse(sessionStorage.getItem('user'));

    return (
      <div className={classes.root}>
        <AppBar position="static" color='primary'>
          <Typography variant="title" color='inherit'>
            <div className='containerHeader'>
              <div className='shopName'>
                TechShop
              </div>
              {user ? (<div className='log'>
                <div> {user['email']} </div>
                <div> / </div>
                <Logout />
              </div>) : <div className='log'><Login /></div>}
            </div>
          </Typography>
          <Tabs classes={{ root: classes.tabRoot, flexContainer: classes.tabsRoot }} indicatorColor="primary" value={this.props.tabValue} onChange={this.handleChange}>
            <Tab label="Home" component={Link} to='/' />
            <Tab label="All Products" component={Link} to='/products' />
            <Tab label="Top" component={Link} to='/top' />
            <Tab label="Sale" component={Link} to='/sale' />
            <Tab label="Forum" component={Link} to='/forum' />
            <Tab label="About" component={Link} to='/about' />
            <Tab label={this.handleCount()} icon={<ShoppingCart />} />
          </Tabs>
        </AppBar>
        <Drawer anchor="right" open={right} onClose={() => { this.toggleDrawer('right', false) }}>
          <Cart />
        </Drawer>
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  tabValue: PropTypes.number.isRequired,
};

export default withStyles(styles)(Header);