import { connect } from 'react-redux';
import Header from './Header';
import { doChangeTab } from '../../modules/header';
import { getCardList } from '../../selectors/bag';

const mapStateToProps = state => ({
    tabValue: state.header.tabValue,
    bag: getCardList(state),
})

const mapDispatchToProps = dispatch => ({
    changeTabValue: (value) => { dispatch(doChangeTab(value)) },
})

export default connect(mapStateToProps, mapDispatchToProps)(Header)

