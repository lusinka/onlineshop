import { connect } from 'react-redux';
import { getTopProduct } from '../../selectors/products';
import Tops from './Tops';

const mapStateToProps = state => ({
    topProduct: getTopProduct(state),
});

export default connect(mapStateToProps)(Tops);

