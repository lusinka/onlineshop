import { connect } from 'react-redux';
import { doAddToCart } from '../../modules/bag';
import Top from './Top';

const mapDispatchToProps = {
    addToCart: doAddToCart,
}

export default connect(null, mapDispatchToProps)(Top);

