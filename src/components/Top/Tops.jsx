import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Top from './Top.container';
import styles from './style';

class Tops extends React.Component {

    render() {
        const { classes, topProduct } = this.props;

        return (
            <div className={classes.container}>
                {topProduct.map(product => (
                    <Top key={product.id} {...product} />
                )
                )}
            </div>
        );
    };
}

Tops.propTypes = {
    classes: PropTypes.object.isRequired,
    topProduct: PropTypes.array.isRequired,
};

export default withStyles(styles)(Tops);
