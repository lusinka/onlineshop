const styles = {

    '@media (min-width: 321px)': {

        container: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            backgroundColor: '#1b1f26',
            width: '100%',
        },
        list: {
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'nowrap',
            width: '100%',
            justifyContent: 'space-around',
        },
        listContainer: {
            display: 'flex',
            flexDirection: 'column',
            paddingTop: 25,
            paddingBottom: 25,
        },
        listItem: {
            textAlign: 'center',
            paddingLeft: 0,
            paddingRight: 0,
        },
        listItemHeader: {
            color: 'white',
            marginTop: 0,
            marginBottom: 0,
            textAlign: 'center',
        },
        listItemContent: {
            textAlign: 'center',
            color: '#d5d5d5',
            '&:hover': {
                textDecoration: 'underline',
            },
        },
        info: {
            width: '100%',
            textAlign: 'center',
        },
        listItemInfo: {
            color: '#d5d5d5',
            textAlign: 'center',
        },
        socIcons: {
            display: 'flex',
            justifyContent: 'center',
            width: '100%',
        },
        socIcon: {
            width: 42,
            height: 40,
            marginRight: 10,
        },
        socIconImg: {
            width: '100%',
            height: '100%',
            opacity: 0.5,
            transition: 'opacity 0.8s',
            '&:hover': {
                opacity: 1,
            },
        },
    },

    '@media (min-width: 769px)': {

        container: {
            flexDirection: 'row',
            flexWrap: 'nowrap',
            justifyContent: 'space-around',
        },
        list: {
            width: '60%',
            justifyContent: 'space-around',
        },
        listContent: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-around',
            alignItems: 'center',
        },
        info: {
            width: '30%',
        },
    },

    '@media (min-width: 1025px)': {

        info: {
            width: '20%',
        },
    },
};

export default styles;
