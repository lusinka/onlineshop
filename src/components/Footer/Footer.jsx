import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { Link } from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import styles from './style';

class Footer extends React.Component {

    render() {
        const { classes } = this.props;

        return (
            <footer className={classes.container}>
                <div className={classes.list}>
                    <List component="ul" className={classes.listContent}>
                        <ListItem className={classes.listItem}>
                            <ListItemText primary="Shop" classes={{ primary: classes.listItemHeader }} />
                        </ListItem>
                        <ListItem className={classes.listItem} component={Link} to='/top'>
                            <ListItemText primary="Top" classes={{ primary: classes.listItemContent }} />
                        </ListItem>
                        <ListItem className={classes.listItem} component={Link} to='/sale'>
                            <ListItemText primary="Sale" classes={{ primary: classes.listItemContent }} />
                        </ListItem>
                    </List>
                    <List component="ul" className={classes.listContent}>
                        <ListItem className={classes.listItem}>
                            <ListItemText primary="Info" classes={{ primary: classes.listItemHeader }} />
                        </ListItem>
                        <ListItem className={classes.listItem} component={Link} to='/about'>
                            <ListItemText primary="About" classes={{ primary: classes.listItemContent }} />
                        </ListItem>
                        <ListItem className={classes.listItem} component={Link} to='/forum'>
                            <ListItemText primary="Forum" classes={{ primary: classes.listItemContent }} />
                        </ListItem>
                    </List>
                    <List component="ul" className={classes.listContent}>
                        <ListItem className={classes.listItem}>
                            <ListItemText primary="Support" classes={{ primary: classes.listItemHeader }} />
                        </ListItem>
                        <ListItem className={classes.listItem} component={Link} to='/helpcenter'>
                            <ListItemText primary="FAQ" classes={{ primary: classes.listItemContent }} />
                        </ListItem>
                        <ListItem className={classes.listItem} component={Link} to='/policy'>
                            <ListItemText primary="Privacy Policy" classes={{ primary: classes.listItemContent }} />
                        </ListItem>
                    </List>
                </div>
                <div className={classes.info}>
                    <List component="ul">
                        <ListItem className={classes.listItem}>
                            <ListItemText primary="Contact" classes={{ primary: classes.listItemHeader }} />
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <ListItemText primary="Customer Service:" classes={{ primary: classes.listItemInfo }} />
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <ListItemText primary="+374 55 522 320" classes={{ primary: classes.listItemInfo }} />
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <ListItemText primary="TechShop@gmail.com" classes={{ primary: classes.listItemInfo }} />
                        </ListItem>
                        <div className={classes.socIcons}>
                            <div className={classes.socIcon}>
                                <a href="https://www.facebook.com/" target="_blank" rel='noopener noreferrer'>
                                    <img className={classes.socIconImg} alt="facebook" src="/assets/img/fb-logo-white.png" />
                                </a>
                            </div>
                            <div className={classes.socIcon}>
                                <a href="https://www.twitter.com/" target="_blank" rel='noopener noreferrer'>
                                    <img className={classes.socIconImg} alt="twitter" src="/assets/img/tw-logo-white.png" />
                                </a>
                            </div>
                            <div className={classes.socIcon}>
                                <a href="https://www.instagram.com/" target="_blank" rel='noopener noreferrer'>
                                    <img className={classes.socIconImg} alt="Instagram" src="/assets/img/insta-logo-white.png" />
                                </a>
                            </div>
                        </div>
                    </List>
                </div>
            </footer>
        );
    }
}

Footer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);


