import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import styles from './style';
import SaleProduct from './SaleProduct';

class Sale extends Component {
    render() {
        const { classes, products } = this.props;

        return (
            <div className={classes.productContainer} >
                {products.map(product => (
                    product.sale ? (<SaleProduct key={product.id} product={product} />) : null
                )
                )}
            </div>
        );
    };
}

Sale.propTypes = {
    classes: PropTypes.object.isRequired,
    products: PropTypes.array.isRequired,
};


export default withStyles(styles)(Sale);

