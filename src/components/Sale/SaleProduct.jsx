import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Dialog from 'material-ui/Dialog';
import DialogActions from 'material-ui/Dialog/DialogActions';
import DialogContent from 'material-ui/Dialog/DialogContent';
import Button from 'material-ui/Button';
import ViewDetail from '../ViewDetail/ViewDetail.container';
import styles from './saleStyle';

class SaleProduct extends React.Component {

    state = {
        open: false,
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    render() {
        const { classes, product } = this.props;

        return (
            <Fragment>
                <Card className={classes.card}>
                    <div className={classes.cardContent}>
                        <CardMedia style={{ width: '70%', height: 230, backgroundSize: 'contain' }} image={product.image_url_big}
                        />
                        <div className={classes.textContent}>
                            <div style={{ flexGrow: 1 }}>
                                <Typography component="p" className={classes.saleContent}>
                                    Sale
                                </Typography>
                            </div>
                            <div className={classes.discount}>
                                <div className={classes.discountContainer} style={{ alignItems: 'flex-end' }}>
                                    <Typography component="p" className={classes.discountText}>
                                        {product.discount}
                                    </Typography>
                                </div>
                                <div className={classes.discountContainer}>
                                    <Typography component="p" className={classes.underlineText}>
                                        {product.price}
                                    </Typography>
                                </div>
                            </div>
                        </div>
                        <div className={classes.quickView}>
                            <Typography component="p" className={classes.viewText} onClick={this.handleClickOpen}>
                                Quick View
                            </Typography>
                        </div>
                    </div>
                    <div className={classes.productDesc}>
                        <Typography component="p" className={classes.textDesc} onClick={this.handleClickOpen}>
                            {product.name}
                        </Typography>
                    </div>
                </Card>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                >
                    <DialogContent>
                        <ViewDetail   {...product} closeDialog={this.handleClose} />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary" autoFocus>
                            Close
                        </Button>
                    </DialogActions>
                </Dialog>
            </Fragment>
        )
    }
}

SaleProduct.propTypes = {
    classes: PropTypes.object.isRequired,
    product: PropTypes.object.isRequired,
};


export default withStyles(styles)(SaleProduct);