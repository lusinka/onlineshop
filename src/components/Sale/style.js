const styles = {

    productContainer: {
        width: '95%',
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        margin: '220px auto 40px',
    },
}

export default styles;