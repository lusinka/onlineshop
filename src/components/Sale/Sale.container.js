import { connect } from 'react-redux';
import Sale from './Sale';
import { getProducts } from '../../selectors/products';

const mapStateToProps = state => ({
    products: getProducts(state),
})

export default connect(mapStateToProps)(Sale);