import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import WOW from 'wowjs';
import Feedback from '../Feedback/Feedback.container';
import styles from './style';

class About extends Component {

    componentDidMount() {
        const wow = new WOW.WOW();
        wow.init();
    };

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.container}>
                <div className={classes.header}>
                </div>
                <div className={`wow slideInDown ${classes.brand}`}>
                    <div className={classes.textFirst} style={{ marginTop: 40, }}>
                        WE ARE THE YOUNG,
                    </div>
                    <div className={classes.textFirst}>
                        INCLUSIVE ONLINE MAGAZINE
                        </div>
                    <div className={classes.textFirst}>
                        THINK OF US AS YOUR
                        </div>
                    <div className={classes.textFirst}>
                        BEST FRIEND
                        </div>
                    <div className={classes.textFirst}>
                        WHO WE ARE?
                        </div>
                    <div className={classes.line}>
                        ____________________________________________
                    </div>
                    <div className={classes.textSecond} style={{ margin: '40px 0 40px', }}>
                        We started in 2018 and haven't stopped smashing it since.
                        We always bring something new with over 100 new products dropping on the daily,
                        bringing you the latest looks for less. We recognize individuality.
                        We see you. We embrace it. We make online magazine accessible and fun
                    </div>
                </div>
                <div className={classes.content}>
                    <div className={classes.philosophyImg}></div>
                    <div className={`wow slideInDown ${classes.philosophy}`}>
                        <div className={classes.textSecond} style={{ marginTop: 40, }}>
                            Our philosophy's pretty simple: we don't take online magazine or life too seriously.
                        </div>
                        <div className={classes.textSecond}>
                            From over 4 million fans on social and an online magazine.
                        </div>
                        <div className={classes.textFirst} style={{ marginTop: 40, textAlign: 'right' }}>
                            WORK HARD,
                        </div>
                        <div className={classes.textFirst} style={{ textAlign: 'right' }}>
                            HAVE FUN,
                        </div>
                        <div className={classes.textFirst} style={{ marginBottom: 40, textAlign: 'right' }}>
                            NO DRAMA.
                        </div>
                    </div>
                </div>
                <div className={classes.animateSection}>
                    <h3 style={{ textAlign: 'center', }}>Our Journey</h3>
                    <div className={classes.verticalLine}>
                        <div className={classes.year}>
                            2018
                        </div>
                        <div className={`wow slideInLeft ${classes.elementsLeft}`}>
                            <div className={classes.circleLeft}>
                            </div>
                            <div className={classes.elementsLeftImg}>
                                <img src='/assets/img/birthday.jpg' alt='birthday' className={classes.image} />
                            </div>
                            <div className={classes.elementsLeftText}>
                                <h2>
                                    MAY 2018
                                </h2>
                                <p style={{ color: '#666666', }}>
                                    We were born
                                </p>
                                <div className={classes.part}>
                                </div>
                            </div>
                        </div>
                        <div className={classes.year} style={{ background: 'rgb(239, 108, 0)', }}>
                            Our Team
                        </div>
                        <div className={`wow slideInRight ${classes.elementsRight}`}>
                            <div className={classes.circleRight}>
                            </div>
                            <div className={classes.elementsRightImg}>
                                <img src='/assets/img/lusine.jpg' alt='founder' className={classes.image} />
                            </div>
                            <div className={classes.elementsRightText}>
                                <h2>
                                    Lusine Iskandaryan
                                </h2>
                                <p className={classes.text}>
                                    Founder
                                    Front End Software Engineer
                                </p>
                            </div>
                        </div>
                        <div className={`wow slideInLeft ${classes.elementsLeft}`}>
                            <div className={classes.circleLeft} style={{ background: '#EF6C00', }}>
                            </div>
                            <div className={classes.elementsLeftImg}>
                                <img src='/assets/img/designer.jpg' alt='designer' className={classes.image} />
                            </div>
                            <div className={classes.elementsLeftText}>
                                <h2>
                                    
                                </h2>
                                <p style={{ color: '#666666', }}>
                                    Designer    
                                </p>
                                <div className={classes.part}>
                                </div>
                            </div>
                        </div>
                        <div className={`wow slideInRight ${classes.elementsRight}`}>
                            <div className={classes.circleRight}>
                            </div>
                            <div className={classes.elementsRightImg}>
                                <img src='/assets/img/manager.png' alt='manager' className={classes.image} />
                            </div>
                            <div className={classes.elementsRightText}>
                                <h2>
                                    
                                </h2>
                                <p className={classes.text}>
                                    Manager
                                </p>
                            </div>
                        </div>
                        <div className={`wow slideInLeft ${classes.elementsLeft}`}>
                            <div className={classes.circleLeft} style={{ background: '#EF6C00', }}>
                            </div>
                            <div className={classes.elementsLeftImg}>
                                <img src='/assets/img/seller.png' alt='seller' className={classes.image} />
                            </div>
                            <div className={classes.elementsLeftText}>
                                <h2>
                                    
                                </h2>
                                <p style={{ color: '#666666', }}>
                                    Seller
                                </p>
                                <div className={classes.part}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 style={{ textAlign: 'center', }}>
                        And the journey continues...
                    </h3>
                </div>
                <Feedback />
            </div >
        );
    };
}

About.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(About);
