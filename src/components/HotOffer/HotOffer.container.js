import { connect } from 'react-redux';
import { getNewProducts } from '../../selectors/products';
import HotOffer from './HotOffer';

const mapStateToProps = state => ({
    products: getNewProducts(state),
});

export default connect(mapStateToProps, null)(HotOffer);