import React from 'react';
import PropTypes from 'prop-types';
import Carousel from 'nuka-carousel';
import TopSale from '../TopSale/TopSale.container';
import NewProduct from '../NewProducts/NewProduct';

const HotOffer = props => {
        const { products } = props;

        return (
                <Carousel speed={1000} autoplay={true} >
                        <TopSale />
                        {products.map(product => <NewProduct product={product} key={product.id} />)}
                </Carousel>
        );
};

HotOffer.propTypes = {
        products: PropTypes.array.isRequired,
};


export default HotOffer;