import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import styles from './styles';




class PrivacyPolicy extends Component {

    render() {
        const { classes } = this.props;
        return (

            <div className={classes.root}>
                <div className={classes.title}>
                    <h1 > Privacy Notice </h1>
                </div>
                <div className={classes.content}>
                    <p>
                        This privacy notice discloses the privacy practices for (website address).
                        This privacy notice applies solely to information collected by this website.
                        It will notify you of the following:
                    </p>
                    <ol>
                        <li>
                            What personally identifiable information is collected from you through the website,
                            how it is used and with whom it may be shared.
                        </li>
                        <li>
                            What choices are available to you regarding the use of your data.
                        </li>
                        <li>
                            The security procedures in place to protect the misuse of your information.
                        </li>
                    </ol>
                    <h2>
                        Information Collection, Use, and Sharing 
                    </h2>
                    <p>
                        We are the sole owners of the information collected on this site.
                        We only have access to/collect information that you voluntarily give us via email or
                        other direct contact from you. We will not sell or rent this information to anyone.
                        We will use your information to respond to you, regarding the reason you contacted us.
                        We will not share your information with any third party outside of our organization,
                        other than as necessary to fulfill your request, e.g. to ship an order.
                        Unless you ask us not to, we may contact you via email in the future to tell you about specials,
                        new products or services, or changes to this privacy policy.
                    </p>
                    <h2>
                        Your Access to and Control Over Information 
                    </h2>
                    <p>
                        You may opt out of any future contacts from us at any time. You can do the following
                        at any time by contacting us via the email address or phone number given on our website:
                    </p>
                    <ul>
                        <li>
                            See what data we have about you, if any.
                        </li>
                        <li>
                            Change/correct any data we have about you.
                        </li>
                        <li>
                            Have us delete any data we have about you.
                        </li>
                        <li>
                            Express any concern you have about our use of your data.
                        </li>

                    </ul>
                    <h2>
                        Security 
                    </h2>
                    <p>
                        We take precautions to protect your information.
                        When you submit sensitive information via the website,
                        your information is protected both online and offline.
                        Wherever we collect sensitive information (such as credit card data),
                        that information is encrypted and transmitted to us in a secure way.
                        You can verify this by looking for a lock icon in the address bar and looking for "https"
                        at the beginning of the address of the Web page.
                        While we use encryption to protect sensitive information transmitted online,
                        we also protect your information offline.
                        The computers/servers in which we store
                        personally identifiable information are kept in a secure environment.
                    </p>
                    <strong className={classes.help}>
                        If you feel that we are not abiding by this privacy policy,
                        you should contact us immediately via telephone at
                        <span className={classes.phone}>  
                            +374 55 522 320
                        </span> 
                            or via 
                        <span className={classes.email}> 
                            OnlineShop@gmail.com
                        </span>.
                    </strong>
                </div>
            </div>
        );
    }
};

PrivacyPolicy.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PrivacyPolicy);