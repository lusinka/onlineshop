const styles = theme => ({

    root: {
        width: '80%',
        margin: '180px auto 40px',
        display: 'flex',
        flexDirection: 'column',
        background: "white",
        fontSize: "20px",
    },

    title: {
        background: "#222426",
        color: "white",
        paddingLeft: 20,
    },

    content: {
        margin: "0 50px",
        marginBottom: "20px",
        color: "#1c1d1e",
    },

    phone: {
        textDecoration: "underline",
        marginLeft: "5px"
    },

    email: {
        textDecoration: "underline"
    }

});

export default styles;
