import React, { Component } from 'react';
import { createMuiTheme, MuiThemeProvider } from 'material-ui/styles';
import { Provider } from 'react-redux';
import { BrowserRouter, } from 'react-router-dom';
import store from '../../store';
import Root from '../Root/Root.container';
import './App.css';

const theme = createMuiTheme({
  typography: {
    baseFontSize: 15,
    fontFamily: 'Montserrat, sans-serif',
  },
  palette: {
    primary: {
      light: '#42464e',
      main: '#1b1f26',
      dark: '#000000',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ffe97d',
      main: '#ffb74d',
      dark: '#c88719',
      contrastText: '#000',
    },
  },
});

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Provider store={store}>
          <MuiThemeProvider theme={theme}>
            <Root />
          </MuiThemeProvider>
        </Provider>
      </BrowserRouter>
    );
  }
}

export default App;
