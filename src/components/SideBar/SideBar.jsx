import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Clear from '@material-ui/icons/Clear';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import './style.css';

const Range = Slider.Range;

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 200,
        height: '100%',
        backgroundColor: '#d9e1e2',
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },
    item: {
        marginTop: 30,
    },
    checkbox: {
        marginLeft: 40,
    },
});

class SideBar extends Component {
    state = {
        ...this.props.filterState,
    }

    componentDidMount() {
        this.props.filtering(this.props.allProducts, this.state);
    }

    onProductType = value => {
        this.setState(
            {
                [value.target.value]: !this.state[value.target.value],
            }, this.updatingCallback)
    }

    onCheckboxChange = (value) => {
        this.setState(
            {
                [value.target.value]: !this.state[value.target.value],
            }, this.updatingCallback)

    }

    onSliderChange = (value) => {
        this.setState({
            minPrice: value[0] + '$',
            maxPrice: value[1] + '$',
        }, this.updatingCallback)
    }

    handleClickProduct = () => {
        this.setState(
            {
                openProduct: !this.state.openProduct,
                openBrand: false,
                openPrice: false,
            }
        )
    }

    handleClickPrice = () => {
        this.setState(
            {
                openPrice: !this.state.openPrice,
                openProduct: false,
                openBrand: false,
                slider: false,
            }
        )
    }

    handleClear = () => {
        this.setState({
            slider: true,
            All: false,
            New: false,
            Sale: false,
            Best: false,
            openProduct: false,
            openPrice: false,
            openBrand: false,
            minPrice: 0 + '$',
            maxPrice: 2500 + '$',
            HP: false,
            ASUS: false,
            LG: false,
            MSI: false,
            CyberpowerPC: false,
            TCL: false,
            'Holy Stone': false,
            Syma: false,
            Apple: false,
        }, this.updatingCallback)
    }

    updatingCallback = () => {
        this.props.filtering(this.props.allProducts, this.state);
    };

    handleClickBrand = () => {
        this.setState(
            {
                openBrand: !this.state.openBrand,
                openProduct: false,
                openPrice: false,
            }
        )
    };

    render() {
        const min = parseInt(this.state.minPrice, 10);
        const max = parseInt(this.state.maxPrice, 10);
        const { classes } = this.props;
        return (
            <div className='sidenav'>
                <div className={classes.root}>
                    <List component="nav">
                        <ListItem text='true'>
                            <ListItemText primary='Shop by' />
                        </ListItem>
                        <ListItem button onClick={this.handleClickProduct}>
                            <ListItemText inset primary="Product" />
                            {this.state.openProduct ? <ExpandLess /> : <ExpandMore />}
                        </ListItem>
                        <Collapse in={this.state.openProduct} timeout="auto" unmountOnExit>
                            <List component="div" disablePadding>
                                <div className='productType'>
                                    <ListItem className={classes.checkbox}>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={this.state.Best}
                                                    onChange={this.onProductType}
                                                    value='Best'
                                                />
                                            }
                                            label='Best'
                                        />
                                    </ListItem>
                                </div>
                                <div className='productType'>
                                    <ListItem className={classes.checkbox}>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={this.state.New}
                                                    onChange={this.onProductType}
                                                    value='New'
                                                />
                                            }
                                            label='New'
                                        />
                                    </ListItem>
                                </div>
                                <div className='productType'>
                                    <ListItem className={classes.checkbox}>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={this.state.Sale}
                                                    onChange={this.onProductType}
                                                    value='Sale'
                                                />
                                            }
                                            label='Sale'
                                        />
                                    </ListItem>
                                </div>
                            </List>
                        </Collapse>
                        <ListItem button onClick={this.handleClickPrice}>
                            <ListItemText inset primary="Price" />
                            {this.state.openPrice ? <ExpandLess /> : <ExpandMore />}
                        </ListItem>
                        <Collapse in={this.state.openPrice} timeout="auto" unmountOnExit={this.state.slider}>
                            <List component="div" disablePadding>
                                <ListItem className={classes.nested}>
                                    <Range allowCross={true}
                                        value={[min, max]}
                                        step={50}
                                        min={0}
                                        max={3000}
                                        //value={[this.state.minPrice,this.state.maxPrice]} 
                                        onChange={this.onSliderChange}
                                    />
                                </ListItem>
                                <ListItem text='true' >
                                    <div className='price'>{`From  ${this.state.minPrice}   -   To  ${this.state.maxPrice}`}</div>

                                </ListItem>
                            </List>
                        </Collapse>
                        <ListItem button onClick={this.handleClickBrand}>
                            <ListItemText inset primary="Brand" />
                            {this.state.openBrand ? <ExpandLess /> : <ExpandMore />}
                        </ListItem>
                        <Collapse in={this.state.openBrand} timeout="auto" unmountOnExit>
                            <List component="div" disablePadding>
                                {this.props.brands.map(brand => {
                                    return (
                                        <div className='brand' key={brand.id}>
                                            <ListItem className={classes.checkbox}>
                                                <FormControlLabel
                                                    control={
                                                        <Checkbox
                                                            checked={!(!this.props.filteredProducts.length || (this.props.filteredProducts.filter(product => product.brand.name === brand.name).length)) ? false : this.state[brand.name]}
                                                            onChange={this.onCheckboxChange}
                                                            value={brand.name}
                                                            disabled={!(!this.props.filteredProducts.length || (this.props.filteredProducts.filter(product => product.brand.name === brand.name).length))}
                                                        />
                                                    }
                                                    label={brand.name}
                                                />
                                            </ListItem>
                                        </div>
                                    )
                                })}


                            </List>
                        </Collapse>
                        <ListItem onClick={this.handleClear} className={classes.item} button >
                            <ListItemIcon>
                                <Clear />
                            </ListItemIcon>
                            <ListItemText inset primary='Clear' />
                        </ListItem>
                    </List>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(SideBar);






