import { connect } from 'react-redux';
import SideBar from './SideBar';
import { getBrands, getProducts, getFilteredProducts } from '../../selectors/products';
import { getFilterState } from '../../selectors/filteredProducts';
import { doFilter } from '../../modules/filteredProducts';

const mapStateTopProps = state => ({
    brands: getBrands(state),
    allProducts: getProducts(state),
    filteredProducts: getFilteredProducts(state),
    filterState: getFilterState(state),
})

const mapDispatchToProps = dispatch => ({
    filtering: (allProducts, filter) => {
        dispatch(doFilter(allProducts, filter))
    },
})

export default connect(mapStateTopProps, mapDispatchToProps)(SideBar)