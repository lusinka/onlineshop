import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { withRouter } from 'react-router-dom';
import Carousel from 'nuka-carousel';
import styles from './styles';

class  TopBrands extends Component {

  handleClick = () => {
    this.props.history.push('./products');
    window.scrollTo(0,0);
  }


 render() {
  const { classes, brands } = this.props;
  return (
    <div className={classes.root}>
      <p className={classes.title}>Top Brands</p>
      <Carousel  slidesToShow={4} autoplay={true} speed={900}>
        {brands.map(brand => {
          return <div key={brand.id} onClick={this.handleClick}>
            <p className={classes.brand}>{brand.name}</p>
          </div>
        })}
      </Carousel>
    </div>
  );
}
};

TopBrands.propTypes = {
  classes: PropTypes.object.isRequired,
  brands: PropTypes.array.isRequired,
};

export default withStyles(styles)(withRouter(TopBrands));
