import { connect } from 'react-redux';
import TopBrands from './TopBrands';
import { getTopBrands, } from '../../selectors/products';

const mapStateToProps = state => ({
    brands: getTopBrands(state),
});

export default connect(mapStateToProps, null)(TopBrands);