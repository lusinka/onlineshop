const styles = theme => ({

    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        borderTop: "5px solid #e2e2e2",
        background: "#f7f7f7",
        fontSize:"40px"
    },
   
    brand: {
        height: 100,
        fontSize:"30px",
        fontWeight:"bold",
        color:"#474747",
        
    },

});

export default styles;