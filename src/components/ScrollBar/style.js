const styles = {

    scrollToTop: {
        position: 'fixed',
        right: 30,
        bottom: 70,
        transition: 'bottom 2s',
        cursor: 'pointer',
        border: '2px solid #6E6E6E',
        width: 50,
        height: 52,
        zIndex: 1,
        textAlign: 'center',
        backgroundColor: 'white',
        '&:hover': {
            backgroundColor: '#d5d5d5',
        },
        color: '#434343',
    },
    scrollImg: {
        width: 30,
    }
}

export default styles;