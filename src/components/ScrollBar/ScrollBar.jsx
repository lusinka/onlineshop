import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import styles from './style';

class ScrollBar extends React.Component {

    state = {
        scroll: false,
    };

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll = (event) => {
        if (window.scrollY === 0) {
            this.setState({
                scroll: false,
            });
            return
        }
        this.setState({
            scroll: true,
        });
    };

    handleClick = (event) => {
        window.scrollTo(0, 0);
    }

    render() {
        const { classes } = this.props;
        const { scroll } = this.state;

        return scroll ? (
            <div className={classes.scrollToTop} onClick={this.handleClick}>
                <img src='/assets/img/up.png' alt='scrollIcon' className={classes.scrollImg} />
                <div>TOP</div>
            </div>
        ) : null
    };
}

ScrollBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ScrollBar);