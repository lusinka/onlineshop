import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getProducts, } from '../../selectors/products';
import { fetchAllProducts } from '../../modules/products';
import Root from './Root.jsx';

const mapStateToProps = state => ({
    products: getProducts(state),
});

const mapDispatchToProps = {
    fetchAllProducts,
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Root));