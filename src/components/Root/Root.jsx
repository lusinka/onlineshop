import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import Header from '../Header/Header.container';
import Footer from '../Footer/Footer';
import ScrollBar from '../ScrollBar/ScrollBar';
import About from '../About/About';
import Forum from '../Forum/Forum.container';
import Sale from '../Sale/Sale.container';
import Tops from '../Top/Tops.container';
import AllProducts from '../AllProducts/AllProducts.container'
import SideBar from '../SideBar/SideBar.container';
import Main from './Main';
import PrivacyPolicy from '../PrivacyPolicy/PrivacyPolicy';
import Faq from '../Faq/Faq';
import styles from './styles'

class Root extends Component {

    componentDidMount() {
        const { fetchAllProducts, } = this.props
        fetchAllProducts();
    };

    handleUpdate = () => {
        let { action } = this.state.location;

        if (action === 'POP') {
            window.scrollTo(0, 0);
        }
    }

    render() {
        const { products, classes } = this.props;

        return (
            <div className={this.props.classes.root} >
                <Header />
                {products.length > 0 ? (<div className={this.props.classes.container}>
                    <Switch onUpdate={this.handleUpdate}>
                        <Route exact path='/products' render={() => { return (<div className={classes.content}><SideBar /><div className={this.props.classes.allProducts}><AllProducts /></div></div>) }} />
                        <Route exact path='/' render={() => {
                            return (<div className={classes.main}><Main /> </div>)
                        }} />
                        <Route exact path='/top' render={() => { return (<Tops />) }} />
                        <Route exact path='/about' render={() => { return (<About />) }} />
                        <Route exact path='/forum' render={() => { return (<Forum />) }} />
                        <Route exact path='/sale' render={() => { return (<Sale />) }} />
                        <Route exact path='/policy' render={() => { return (<PrivacyPolicy />) }} />
                        <Route exact path='/helpcenter' render={() => { return (<Faq />) }} />
                    </Switch>
                    <ScrollBar />
                </div>) : <div className={classes.containerCircular}>
                        <div className={classes.circular}><CircularProgress color='secondary' size={50} /></div>
                    </div>}
                <Footer />
            </div>
        );
    }
};


Root.propTypes = {
    classes: PropTypes.object.isRequired,
    products: PropTypes.array.isRequired,
};

export default withStyles(styles)(Root);


