import React, { Fragment } from 'react';
import BestSellers from '../BestSellers/BestSellers.container';
import TopBrands from '../TopBrands/TopBrands.container';
import HotOffer from '../HotOffer/HotOffer.container';

const Main = props => {
    return (
        <Fragment>
            <div>
                <HotOffer />
            </div>
            <div >
                <BestSellers />
            </div>
            <div >
                <TopBrands />
            </div>
        </Fragment>
    );
};

export default Main;