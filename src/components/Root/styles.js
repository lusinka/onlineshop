

const styles = theme => ({
    root: {
        'overflow-x': 'hidden',
        margin: '0',
        padding: '0',
        'font-family': 'sans-serif',
        display: "flex",
        flexDirection: "column"
    },
    topProduct: {
        'margin-top': '200px',
        'margin-bottom': '50px',
    },
    container: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
    },
    containerCircular: {
        'min-height': 'calc(100vh - 248px)',
        'overflow-y': 'hidden',
    },

    allProducts: {
        display: 'flex',
        flexDirection: 'column',
        width: '75%',
    },

    main: {
        marginTop: "150px"
    },
    circular: {
        'margin-top': '300px',
        display: 'flex',
        'justify-content': 'center',
        margin: 'auto',
    },

    topbrands: {
        marginBottom: "20px",
    },
    content: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        width: '100%',
    },
});

export default styles;