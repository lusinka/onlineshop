const styles = theme => ({

    card: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%',
        transition: 'width 1s ease',
        cursor: 'pointer',
        background: '#f8f8f8',
        overflow: 'hidden',
    },
    content: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        width: '50%',
        alignItems: 'center',
    },
    image: {
        width: '100%', 
        height: 300,
    },
    text: {
        color: '#434343',
        fontSize: 16,
        fontWeight: 500,
        fontFamily: 'Montserrat, sans-serif',
        lineHeight: 1.1,
        textAlign: 'left',
    },
    counter: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    button: {
        margin: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    priceText: {
        display: 'flex',
        justifyContent: 'flex-start',
        width: '100%',
        fontSize: 32,
        fontWeight: 'bolder',
        color: '#303030',
        padding: '15px 0',
    },
    countText: {
        fontSize: 24,
        fontWeight: 'bolder',
        color: '#303030',
    },
    addToBug: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
    },
    addButton: {
        backgroundColor: '#ffb74d',
        fontWeight: 'bold',
        '&:hover': {
            backgroundColor: '#1b1f26',
            color: 'white',
        },
        fontSize: "12px",
        borderRadius: "5px",
        boxShadow: "0 8px 16px 0 rgba(0,0,0,0.1), 0 6px 20px 0 rgba(0,0,0,0.19)",
    },
});
 
export default styles;