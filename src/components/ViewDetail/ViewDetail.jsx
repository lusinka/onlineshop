import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import styles from './styles';

class ViewDetail extends Component {

    state = {
        count: 1,
    };

    handleAddToCart = (event) => {
        const { closeDialog, addToCart, ...product } = this.props;
        closeDialog();
        addToCart({ ...product, count: this.state.count });
    };

    handleDecrement = (event) => {
        if (this.state.count !== 1) {
            this.setState({
                count: this.state.count - 1,
            })
        } else {
            return;
        }
    };

    handleIncrement = (event) => {
        this.setState({
            count: this.state.count + 1,
        })
    };

    render() {
        const { classes, name, description, price, image_url_small, discount } = this.props;
        const { count } = this.state;

        return (
            <Card className={classes.card}>
                <CardContent className={classes.content}>
                    <CardMedia className={classes.image} image={image_url_small}
                    />
                    <Typography component="p" className={classes.text}>
                        {name}
                    </Typography>
                </CardContent>
                <div className={classes.content}>
                    <Typography component="p" className={classes.text}>
                        {description.slice(0, 300) + '...'}
                    </Typography>
                    <Typography component="p" className={classes.priceText}>
                        {discount ? discount : price}
                    </Typography>
                    <CardActions className={classes.counter}>
                        <Button onClick={this.handleIncrement} variant="fab" mini color="primary" className={classes.button}>
                            <AddIcon />
                        </Button>
                        <Typography component="p" className={classes.countText}>
                            {count}
                        </Typography>
                        <Button onClick={this.handleDecrement} variant="fab" mini color="primary" className={classes.button}>
                            <RemoveIcon />
                        </Button>
                    </CardActions>
                    <CardActions className={classes.addToBug}>
                        <Button className={classes.addButton} onClick={this.handleAddToCart} >
                            Add to Cart
                        </Button>
                    </CardActions>
                </div>
            </Card >
        );
    }
};

ViewDetail.propTypes = {
    classes: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    image_url_small: PropTypes.string.isRequired,
    discount: PropTypes.string,

};


export default withStyles(styles)(ViewDetail);