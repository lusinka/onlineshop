import { connect } from 'react-redux';
import ViewDetail from './ViewDetail';
import { doAddToCart } from '../../modules/bag';

const mapDispatchToProps = {
    addToCart: doAddToCart,
}

export default connect(null, mapDispatchToProps)(ViewDetail);