import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import styles from './style';

class CartItem extends React.Component {

    handleDecrement = () => {
        const { bagProduct, decrement } = this.props;
        if (bagProduct.count > 1) {
            decrement(bagProduct);
        }
    };

    render() {
        const { bagProduct, remove, increment, classes } = this.props;

        return (
            <Card className={classes.card}>
                <div className={classes.cardContent}>
                    <CardMedia className={classes.cardImg} image={bagProduct.image_url_small} />
                    <div className={classes.textContent}>
                        <div className={classes.deleteButton}>
                            <IconButton className={classes.button} aria-label="Delete" onClick={() => remove(bagProduct)}>
                                <DeleteIcon color='primary' />
                            </IconButton>
                        </div>
                        <div className={classes.counter}>
                            <div className={classes.text}>Qty</div>
                            <Button onClick={() => increment(bagProduct)} variant="fab" mini color="primary" className={classes.button}>
                                <AddIcon />
                            </Button>
                            <Typography component="p" className={classes.priceText}>
                                {bagProduct.count}
                            </Typography>
                            <Button onClick={this.handleDecrement} variant="fab" mini color="primary" className={classes.button}>
                                <RemoveIcon />
                            </Button>
                        </div>
                        <div className={classes.priceContainer}>
                            <div className={classes.text}>Price</div>
                            <Typography component="p" className={classes.priceText}>
                                {bagProduct.discount ? bagProduct.discount : bagProduct.price}
                            </Typography>
                        </div>
                        <div className={classes.priceContainer}>
                            <div className={classes.text}>Total</div>
                            <Typography component="p" className={classes.priceText}>
                                {(bagProduct.discount ? (parseInt(bagProduct.discount, 10) * bagProduct.count) : (parseInt(bagProduct.price, 10) * bagProduct.count)) + '$'}
                            </Typography>
                        </div>
                    </div>
                </div>
                <div className={classes.detail}>
                    <div className={classes.text}>Product details</div>
                    <Typography component="p" className={classes.textDesc}>
                        {bagProduct.name.slice(0, 40) + '...'}
                    </Typography>
                </div>
            </Card>
        );
    };
};

CartItem.propTypes = {
    classes: PropTypes.object.isRequired,
    bagProduct: PropTypes.object.isRequired,
    remove: PropTypes.func.isRequired,
    increment: PropTypes.func.isRequired,
};

export default withStyles(styles)(CartItem);