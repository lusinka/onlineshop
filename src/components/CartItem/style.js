const styles = theme => ({

    card: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        marginBottom: 20,
        transition: 'width 1s ease',
        cursor: 'pointer',
        background: '#f8f8f8',
        overflow: 'hidden',
    },
    cardContent: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    cardImg: {
        width: '100%',
        backgroundSize: 'cover',
    },
    textContent: {
        width: '70%',
        display: 'flex',
        flexDirection: 'column',
    },
    button: {
        margin: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    deleteButton: {
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'right',
    },
    priceContainer: {
        display: 'flex',
        flexDirection: 'row',
        marginLeft: 20,
        borderTop: '2px solid #e0e0e0',
        justifyContent: 'flex-start',
    },
    text: {
        borderRight: '2px solid #e0e0e0',
        width: '30%',
        paddingTop: 15,
    },
    priceText: {
        fontSize: 16,
        fontWeight: 'bolder',
        color: '#303030',
        paddingTop: 15,
        paddingLeft: 5,
    },
    counter: {
        display: 'flex',
        flexDirection: 'row',
        marginLeft: 20,
        borderTop: '2px solid #e0e0e0',
        justifyContent: 'flex-start',
    },
    detail: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        borderTop: '2px solid #e0e0e0',
    },
    textDesc: {
        color: '#434343',
        fontSize: 16,
        fontWeight: 500,
        lineHeight: 1.1,
        paddingLeft: 10,
        paddingTop: 15,
    },
})

export default styles;