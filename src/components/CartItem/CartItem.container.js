import { connect } from 'react-redux';
import CartItem from './CartItem';
import { remove, increment, decrement } from '../../modules/bag';

const mapDispatchToProps = {
    remove,
    increment,
    decrement,
}

export default connect(null, mapDispatchToProps)(CartItem);