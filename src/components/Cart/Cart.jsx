import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { withStyles } from 'material-ui/styles';
import CartItem from '../CartItem/CartItem.container';
import style from './style'

class Cart extends Component {

    handleBuy = (event) => {
        const { doBuy } = this.props;
        doBuy();
    }

    handleTotalPayable = () => {
        const { bagProducts } = this.props;
        return bagProducts.reduce((sum, product) =>
            sum = sum + (product.discount ?
                (parseInt(product.discount, 10) * product.count) :
                (parseInt(product.price, 10) * product.count)), 0);
    };

    handleTotalItems = () => {
        const { bagProducts } = this.props;
        return bagProducts.reduce((sum, product) => sum = sum + product.count, 0);
    };

    render() {
        const { classes } = this.props;
        const { bagProducts } = this.props;

        return (
            <div className={classes.container}>
                <h1 className={classes.header}>
                    MY BAG
                </h1>
                {bagProducts.length ? (
                    <div className={classes.CartContainer}>
                        <div className={classes.ProductContainer}>
                            {bagProducts.map(bagProduct =>
                                <CartItem key={bagProduct.id} bagProduct={bagProduct}
                                />)}
                        </div>
                        <div className={classes.summery}>
                            <div className={classes.header}>
                                ORDER SUMMARY
                            </div>
                            <div style={{ textAlign: 'center' }}>
                                {this.handleTotalItems()}
                                <span className={classes.text}>
                                    items
                                </span>
                            </div>
                            <div className={classes.header}>
                                TOTAL PAYABLE
                                <span className={classes.headerText}>
                                    {this.handleTotalPayable() + '$'}
                                </span>
                            </div>
                            <Button className={classes.button} color='primary' onClick={this.handleBuy}>
                                PLACE ORDER
                            </Button>
                        </div>
                    </div>
                ) : (
                        <div className={classes.emptyBug}>
                            YOUR SHOPPING BAG IS EMPTY
                </div>
                    )}
            </div>
        );
    }
}

Cart.propTypes = {
    classes: PropTypes.object.isRequired,
    bagProducts: PropTypes.array.isRequired,
};

export default withStyles(style)(Cart);