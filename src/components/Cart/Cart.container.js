import { connect } from 'react-redux';
import Cart from './Cart';
import { getCardList } from '../../selectors/bag';
import { buy } from '../../modules/bag';

const mapStateToProps = state => ({
    bagProducts: getCardList(state),
});

const mapDispatchToProps = {
    doBuy: buy,
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);