const styles = theme => ({

    '@media (min-width: 321px)': {

        container: {
            width: '300px',
            fontFamily: 'Montserrat, sans-serif',
        },
        header: {
            display: 'flex',
            'justify-content': 'center',
            borderBottom: '2px solid #e0e0e0',
            fontWeight: 'bold',

        },
        CartContainer: {
            display: 'flex',
            flexDirection: 'column',
            border: '2px solid #e0e0e0',
            alignItems: 'center',
        },
        ProductContainer: {
            display: 'flex',
            order: 2,
            flexDirection: 'column',
            width: '90%',
            transition: 'width 1s ease',
            marginTop: 40,
        },
        summery: {
            width: '90%',
            height: '40%',
            transition: 'width 1s ease',
            display: 'flex',
            order: 1,
            flexDirection: 'column',
            border: '2px solid #e0e0e0',
            lineHeight: '1.8',
            backgroundColor: '#f8f8f8',
            marginTop: 40,
        },
        text: {
            paddingLeft: 10,
        },
        headerText: {
            paddingLeft: 10,
            fontWeight: 'bold',
            fontFamily: 'Montserrat, sans-serif',
        },
        button: {
            backgroundColor: '#ffb74d',
            fontWeight: 'bold',
            '&:hover': {
                backgroundColor: '#1b1f26',
                color: 'white',
            }
        },
        emptyBug: {
            display: 'flex',
            'justify-content': 'center',
            fontWeight: 'bold',
            alignItems: 'center',
            marginTop: 80,
        }
    },

    '@media (min-width: 569px)': {

        container: {
            width: '400px',
        },
    },

    '@media (min-width: 769px)': {

        container: {
            width: '700px',
        },
        CartContainer: {
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'inherit',
        },
        ProductContainer: {
            width: '50%',
            order: 1,
        },
        summery: {
            width: '40%',
        },
    },

});

export default styles;
