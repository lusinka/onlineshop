import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { Field } from 'redux-form';
import TextField from 'material-ui/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import styles from './style';

const InputTextDate = ({ input, type, meta: { touched, error } }) => (
    <div style={{ margin: 20 }}>
        <TextField
            {...input}
            type={type}
            InputProps={{
                disableUnderline: true,
            }}
        />
        {touched && error && <div style={{ color: 'red' }}>{error}</div>}
    </div>
);

const InputTextField = ({ input, label, type, meta: { touched, error } }) => (
    <div style={{ margin: 20 }}>
        <TextField
            {...input}
            label={label}
            type={type}
        />
        {touched && error && <div style={{ color: 'red' }}>{error}</div>}
    </div>
);

const InputTextArea = ({ input, label, type, meta: { touched, error } }) => (
    <div style={{ margin: 20 }}>
        <textarea {...input} placeholder={label} rows="3" cols="24" />
        {touched && error && <div style={{ color: 'red' }}>{error}</div>}
    </div>
);

class Feedback extends Component {

    state = {
        scroll: false,
        open: false,
    };

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll = (event) => {
        if (window.scrollY < 330) {
            this.setState({
                scroll: false,
            });
            return;
        }
        this.setState({
            scroll: true,
        });
        return;
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        const { reset } = this.props;
        this.setState({ open: false });
        reset();
    };

    handleSubmitForm = (values) => {
        const { doSubmit } = this.props;
        doSubmit(values);
        this.handleClose();
    };

    render() {
        const { classes, reset, submitting, handleSubmit, pristine } = this.props;
        const { scroll, open } = this.state;

        return (
            <div>
                {scroll && <div className={classes.feedback} onClick={this.handleClickOpen}>
                    Feedback
                </div>}
                {open && <Dialog style={{ fontFamily: 'Montserrat, sans-serif', }}
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">
                        What's on your mind? Tell us how we did.
                    </DialogTitle>
                    <DialogContent>
                        <form onSubmit={handleSubmit(this.handleSubmitForm)}>
                            <Field component={InputTextDate} type="date" name="date" />
                            <Field component={InputTextField} type="text" name="name" label="Name" />
                            <Field component={InputTextField} type="email" name="email" label="Email" />
                            <Field component={InputTextArea} name="notes" label="Notes" />
                            <div className={classes.buttons}>
                                <Button type="submit" disabled={submitting} >
                                    Save
                                </Button>
                                <Button type="button" disabled={pristine || submitting} onClick={reset} color="primary">
                                    Reset
                                </Button>
                                <Button type="button" onClick={this.handleClose} color="primary">
                                    Cancel
                                </Button>
                            </div>
                        </form>
                    </DialogContent>
                </Dialog>}
            </div>
        )
    };
}

Feedback.propTypes = {
    classes: PropTypes.object.isRequired,
    reset: PropTypes.func.isRequired,
    pristine: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired,
};

export default withStyles(styles)(Feedback);