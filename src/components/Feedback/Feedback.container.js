import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { sendFeedback } from '../../modules/feedback';
import Feedback from './Feedback';
import validate from './validate';

const mapDispatchToProps = {
    doSubmit: sendFeedback,
}

const formFeedback = reduxForm({
    form: 'feedback',
    validate,
});

const connectFeedback = connect(null, mapDispatchToProps);

export default connectFeedback(formFeedback(Feedback));
