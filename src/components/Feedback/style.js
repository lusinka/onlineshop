const styles = theme => ({

    feedback: {
        position: 'fixed',
        right: 0,
        bottom: 331,
        height: 150,
        width: 35,
        zIndex: 1,
        transition: 'boxShadow 0.1s ease-in-out',
        cursor: 'pointer',
        border: '2px solid #f44336',
        paddingLeft: 14,
        textAlign: 'center',
        backgroundColor: '#ffb74d',
        letterSpacing: 'normal',
        overflowWrap: 'normal',
        wordBreak: 'normal',
        wordWrap: 'normal',
        whiteSpace: 'nowrap',
        writingMode: 'vertical-lr',
        transform: 'rotate(180deg)',
        '&:hover': {
            backgroundColor: '#ef6c00',
            boxShadow: '0 0 35px 2px rgba(0,0,0,.24)',
        },
        color: 'white',
    },
    buttons: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        fontFamily: 'Montserrat, sans-serif',
    },
})

export default styles;