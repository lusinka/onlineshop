import { connect } from 'react-redux';
import TopSale from './TopSale';
import { getTopSale } from '../../selectors/products';

const mapStateToProps = state => ({
    product: getTopSale(state),

});

export default connect(mapStateToProps, null)(TopSale);