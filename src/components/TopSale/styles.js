const styles = theme => ({

  root: {
    height: "380px",
    display: 'flex',
    flexDirection: 'row',
    justifyContent: "center",
    background: "#edf1f7",
  },
  offer: {
    height: "100%",
    width: "75%"
  },
  text: {
    "font-size": "30px",
  },
  salePrice: {
    color: "red",
    marginLeft: "20px",
    "font-size": "50px"
  },
  product: {
    display: "flex",
    justifyContent: "flex-end",
  },
  image: {
    background: "#edf1f7",
  }
});

export default styles;

