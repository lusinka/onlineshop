import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import styles from './styles';

const TopSale = props => {

    const { classes, product } = props;
    return (
        <div className={classes.root}>
            <div className={classes.saleImage}>
                <img src='/assets/img/offer.png' alt='offerImg' className={classes.offer} />
            </div>
            <div className={classes.text}>
                <p>{product.name.slice(0, 25)}</p>
                <strike>{product.price}</strike>
                <span className={classes.salePrice}>{product.discount}</span>
            </div>
            <div className={classes.product}>
                <img className={classes.image} src={product.image_url_small} alt='productImg' />
            </div>
        </div>
    )
};

TopSale.propTypes = {
    classes: PropTypes.object.isRequired,
    product: PropTypes.object.isRequired,
};

export default withStyles(styles)(TopSale);