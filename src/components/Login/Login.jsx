import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { withStyles } from 'material-ui/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import styles from './style';

const InputTextField = ({ input, label, type, meta: { touched, error }, }) => (
    <div style={{ marginBottom: 40, }}>
        <TextField
            style={{ width: '100%' }}
            {...input}
            label={label}
            type={type}
        />
        {touched && error && <div style={{ color: 'red' }}>{error}</div>}
    </div>
);

class Login extends React.Component {

    handleSubmitForm = (values) => {
        const { doSubmit } = this.props;
        const { doClose } = this.props;
        doSubmit(values);
        doClose();
    };

    render() {
        const { classes, handleSubmit, submitting, pristine, doOpen, doClose, open } = this.props;
        return (
            <div className={classes.container}>
                <span className='login' style={{ fontSize: 16, }} onClick={doOpen}> LOG IN </span>
                {open && <Dialog style={{ fontFamily: 'Montserrat, sans-serif', }}
                    open={open}
                    onClose={doClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle classes={{ root: classes.dilogTitle }} id="form-dialog-title">
                        USER LOGIN
                    </DialogTitle>
                    <DialogContent>
                        <form className={classes.form} onSubmit={handleSubmit(this.handleSubmitForm)}>
                            <Field component={InputTextField} type="email" name="email" label="Email" style={{ width: '100%' }} />
                            <Field component={InputTextField} type='password' name="password" label="Password" />
                            <button className={classes.button}
                                type="submit" disabled={pristine || submitting} >
                                LOG IN
                            </button>
                        </form>
                    </DialogContent>
                </Dialog>}
            </div>
        );
    };
};

Login.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);
