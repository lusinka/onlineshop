const styles = theme => ({

    container: {
        width: '35%',
        justifyContent: 'flex-end',
        display: 'flex',
        cursor: 'pointer',
        paddingRight: 10,
    },
    login: {
        cursor: 'pointer',
        padding: '0 10px 0',
    },
    form: {
        height: 400,
        width: 340,
        backgroundColor: '#fff',
    },
    inputField: {
        width: '100%',
    },
    dilogTitle: {
        textAlign: 'center',
    },
    button: {
        marginTop: 60,
        height: 40,
        lineHeight: '29px',
        width: '100%',
        backgroundColor: '#ffb74d',
        color: 'black',
        fontWeight: 'bold',
        '&:hover': {
            backgroundColor: '#1b1f26',
            color: 'white',
        },
        fontSize: "15px",
        borderRadius: "5px",
        boxShadow: "0 8px 16px 0 rgba(0,0,0,0.1), 0 6px 20px 0 rgba(0,0,0,0.19)",
    },
});

export default styles;