import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { fetchLogin } from '../../mockable';
import { doClose, doOpen, } from '../../modules/login';
import { getLoginDialog } from '../../selectors/login';
import { open } from '../../selectors/login';
import Login from './Login';
import validate from './validate';

const mapStateToProps = state => ({
    open: getLoginDialog(state),
})

const mapDispatchToProps = {
    doClose,
    doOpen,
}

const formLogin = reduxForm({
    form: 'login',
    doSubmit: (values, dispatch, ) => fetchLogin(values).then(({ message }) => {
        sessionStorage.setItem('user', JSON.stringify(values));
        alert(message['msg']);
        return message;
    }).catch((error) => {
        alert('Something went wrong!')
    }),
    validate,
});

const connectLogin = connect(mapStateToProps, mapDispatchToProps);

export default connectLogin(formLogin(Login));
