const styles = theme => ({

    root: {
        height: "380px",
        display: 'flex',
        justifyContent: "center",
        alignItems: 'center',
        background: "white",
        width: '100%',
    },
    new: {
        width: "25%",
    },
    offer: {
        height: "50%",
        width: "70%"
    },
    text: {
        "font-size": "30px",
        marginRight: "20px"
    },

});

export default styles;