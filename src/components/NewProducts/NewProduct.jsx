import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import styles from './styles';

const NewProduct = props => {

    const { classes, product } = props;
    return (
        <div className={classes.root}>
            <div className={classes.new}>
                <img src='/assets/img/new.png' alt='offerImage' className={classes.offer} />
            </div>
            <div className={classes.text}>
                <p>{product.name.slice(0, 20) + '...'}</p>
                <p>{product.price}</p>
            </div>
            <div className={classes.product}>
                <img className={classes.image} src={product.image_url_small} alt='productImage' />

            </div>
        </div>
    )
};

NewProduct.propTypes = {
    classes: PropTypes.object.isRequired,
    product: PropTypes.object.isRequired,
};

export default withStyles(styles)(NewProduct);



