import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import BestSeller from './BestSeller';
import styles from './styles';

class BestSellers extends Component {

    render() {

        const { classes, bestSellers } = this.props;
        return (
            <Fragment>
                <div className={classes.root}>
                    <div className={classes.title} >
                        <p> Our BestSellers </p>
                    </div>
                    <div className={classes.container}>
                        {bestSellers.map(product => (
                            <BestSeller key={product.id} product={product} />
                        )
                        )}
                    </div>
                </div>
            </Fragment>
        )
    }
}

BestSellers.propTypes = {
    classes: PropTypes.object.isRequired,
    bestSellers: PropTypes.array.isRequired,
};

export default withStyles(styles)(BestSellers);