import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Dialog from 'material-ui/Dialog';
import DialogActions from 'material-ui/Dialog/DialogActions';
import DialogContent from 'material-ui/Dialog/DialogContent';
import Button from 'material-ui/Button';
import ViewDetail from '../ViewDetail/ViewDetail.container';
import styles from './bestSellerStyle';

class BestSeller extends Component {

    state = {
        open: false,
    };

    handleClickOpen = (event) => {
        this.setState({
            open: true,
        })
    };

    handleClose = (event) => {
        this.setState({
            open: false
        });
    };

    render() {
        const { classes, product } = this.props;

        return (
            <Fragment>
                <Card className={classes.card}>
                    <Typography component="p" className={classes.text}>
                        {product.name.slice(0, 15) + '...'}
                    </Typography>
                    <CardMedia className={classes.image} image={product.image_url_small}
                    />
                    <div className={classes.quickView}>
                        <Typography component="p" className={classes.viewText} onClick={this.handleClickOpen}>
                            Quick View
                        </Typography>
                    </div>
                </Card>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                >
                    <DialogContent>
                        <ViewDetail   {...product} closeDialog={this.handleClose} />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary" autoFocus>
                            Close
                        </Button>
                    </DialogActions>
                </Dialog>
            </Fragment>
        );
    }
}

BestSeller.propTypes = {
    classes: PropTypes.object.isRequired,
    product: PropTypes.object.isRequired,
};

export default withStyles(styles)(BestSeller);