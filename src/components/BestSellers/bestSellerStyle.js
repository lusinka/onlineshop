const styles = theme => ({

    '@media (min-width: 321px)': {
        card: {
            width: '90%',
            marginBottom: 20,
            transition: 'width 1s ease',
            cursor: 'pointer',
            overflow: 'hidden',
            display: 'flex',
            flexDirection: "column",
            alignItems: "center",
            position: 'relative',
            '&:hover': {
                boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
            },
            '&:hover $quickView': {
                opacity: 1,
            },
        },
        text: {
            paddingTop: 10,
            color: '#434343',
            fontSize: 25,
            fontWeight: 500,
            fontFamily: 'Montserrat, sans-serif',
            lineHeight: 1.1,
            textAlign: 'center',
        },
        image: {
            width: 300,
            height: 300,
            backgroundSize: 'contain'
        },
        quickView: {
            position: 'absolute',
            bottom: '10px',
            overflow: 'hidden',
            textAlign: 'center',
            width: '100%',
            height: 50,
            cursor: 'pointer',
            background: 'rgba(255,255,255,0.97)',
            zoom: 1,
            opacity: 0,
            marginBottom: '20%',
        },
        viewText: {
            fontFamily: 'Montserrat, sans-serif',
            fontWeight: 'bolder',
            margin: '15px 0',
            color: '#434343',
        },
    },

    '@media (min-width: 569px)': {

        card: {
            width: '45%',
        },
    },

    '@media (min-width: 769px)': {

        card: {
            width: '30%',
        },
    },

    '@media (min-width: 1025px)': {

        card: {
            width: '22%',
        },
    }

})

export default styles;