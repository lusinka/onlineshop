import { connect } from 'react-redux';
import BestSellers from './BestSellers';
import { getBestSellers, } from '../../selectors/products';

const mapStateToProps = state => ({
    bestSellers: getBestSellers(state),
});

export default connect(mapStateToProps, null)(BestSellers);