const styles = {

    '@media (min-width: 321px)': {

        card: {
            width: '90%',
            marginBottom: 20,
            transition: 'width 1s ease',
            cursor: 'pointer',
            background: '#f8f8f8',
            overflow: 'hidden',
            '&:hover': {
                boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
            },
            '&:hover $quickView': {
                opacity: 1,
            },
        },
        cardContent: {
            position: 'relative',
        },
        mediaImg: {
            width: '100%',
        },
        textContent: {
            width: '100%',
            height: '100%',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end',
            position: 'absolute',
            top: 0,
        },
        discount: {
            width: '30%',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'flex-end',
        },
        saleContent: {
            color: '#d3145a',
            fontSize: 12,
            zIndex: 1,
            textAlign: 'center',
            width: 25,
            height: 17,
        },
        newContent: {
            color: '#346ef6',
            border: '1px solid #346ef6',
            fontSize: 12,
            zIndex: 1,
            textAlign: 'center',
            width: 25,
            height: 17,
        },
        priceContainer: {
            width: '100%',
            height: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
        },
        discountContainer: {
            width: '100%',
            height: '50%',
            display: 'flex',
            alignItems: 'flex-start',
            justifyContent: 'flex-end',
        },
        discountText: {
            fontSize: 16,
            fontWeight: 'bolder',
            color: '#d3145a',
        },
        priceText: {
            fontSize: 16,
            fontWeight: 'bolder',
            color: '#303030',
        },
        underlineText: {
            fontSize: 16,
            fontWeight: 'bolder',
            color: '#303030',
            textDecoration: 'line-through',
        },
        quickView: {
            position: 'absolute',
            bottom: 0,
            overflow: 'hidden',
            textAlign: 'center',
            width: '100%',
            height: 50,
            cursor: 'pointer',
            background: 'rgba(255,255,255,0.97)',
            zoom: 1,
            opacity: 0,
        },
        viewText: {
            fontFamily: 'Montserrat, sans-serif',
            fontWeight: 'bolder',
            margin: '15px 0',
            color: '#434343',
        },
        productDesc: {
            margin: '10px auto',
        },
        textDesc: {
            color: '#434343',
            fontSize: 16,
            fontWeight: 500,
            fontFamily: 'Montserrat, sans-serif',
            lineHeight: 1.1,
            paddingLeft: 10,
        },
    },

    '@media (min-width: 569px)': {

        card: {
            width: '45%',
        },
        newContent: {
            fontSize: 14,
            fontWeight: 'bolder',
            width: 35,
            height: 23,
            paddingTop: 5,
        },
        saleContent: {
            fontWeight: 'bolder',
            width: 35,
            height: 23,
            paddingTop: 5,
            fontSize: 14,
        },
        priceText: {
            fontSize: 20,
        },
    },

    '@media (min-width: 769px)': {

        card: {
            width: '30%',
        },
    },

    '@media (min-width: 1025px)': {

        card: {
            width: '22%',
        },
        saleContent: {
            width: 30,
            height: 20,
            padding: '10px 10px',
        },
        newContent: {
            width: 30,
            height: 20,
            padding: '10px 10px',
        },
        priceText: {
            fontSize: 24,
        },
    }
}

export default styles;