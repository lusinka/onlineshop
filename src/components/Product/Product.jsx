import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Dialog from 'material-ui/Dialog';
import DialogActions from 'material-ui/Dialog/DialogActions';
import DialogContent from 'material-ui/Dialog/DialogContent';
import withMobileDialog from 'material-ui/Dialog/withMobileDialog';
import Button from 'material-ui/Button';
import ViewDetail from '../ViewDetail/ViewDetail.container';
import styles from './style';

class Product extends React.Component {
    state = {
        open: false,
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    render() {
        const { classes, product } = this.props;

        return (
            <Fragment>
                <Card className={classes.card}>
                    <div className={classes.cardContent}>
                        <CardMedia style={{ width: '70%', height: 230, backgroundSize: 'contain' }} image={product.image_url_big}
                        />
                        <div className={classes.textContent}>
                            {(product.sale || product.new) && <div style={{ flexGrow: 1 }}>
                                <Typography component="p" className={product.sale ? classes.saleContent : classes.newContent}>
                                    {product.sale ? 'Sale' : 'New'}
                                </Typography>
                            </div>}
                            <div className={classes.discount}>
                                {product.sale && <div className={classes.discountContainer} style={{ alignItems: 'flex-end' }}>
                                    <Typography component="p" className={classes.discountText}>
                                        {product.discount}
                                    </Typography>
                                </div>}
                                <div className={product.sale ? classes.discountContainer : classes.priceContainer}>
                                    <Typography component="p" className={product.sale ? classes.underlineText : classes.priceText}>
                                        {product.price}
                                    </Typography>
                                </div>
                            </div>
                        </div>
                        <div className={classes.quickView}>
                            <Typography component="p" className={classes.viewText} onClick={this.handleClickOpen}>
                                Quick View
                            </Typography>
                        </div>
                    </div>
                    <div className={classes.productDesc}>
                        <Typography component="p" className={classes.textDesc}>
                            {product.name.slice(0, 40) + '...'}
                        </Typography>
                    </div>

                </Card>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                >
                    <DialogContent>
                        <ViewDetail   {...product} closeDialog={this.handleClose} />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary" autoFocus>
                            Close
                        </Button>
                    </DialogActions>
                </Dialog>
            </Fragment>
        )
    }
}

Product.propTypes = {
    classes: PropTypes.object.isRequired,
    product: PropTypes.object.isRequired,
};

export default withMobileDialog()(withStyles(styles)(Product));
