export const getLoginDialog = (state) => state.login.open;
export const getUser = (state) => state.login.user;
