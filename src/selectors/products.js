export const getProducts = (state) => state.products.data;
export const getError = (state) => state.products.error;
export const getIsLoading = (state) => state.products.isLoading;
export const getBrands = (state) => state.products.brands;
export const getFilteredProducts = state => state.filteredProducts.firstFilter;
export const getAllFilteredProducts = state => state.filteredProducts.data;
export const getTopBrands = state => getBrands(state).filter(brand => brand.top);
export const getBestSellers = state => state.products.data.filter(product => product.bestSeller);
export const getSaledProducts = state => getProducts(state).filter(product => product.sale);
export const getTopSale = state => { 
    return getSaledProducts(state).sort((a, b) => parseInt(a.discount, 10) - parseInt(b.discount, 10))[0] 
};
export const getTopProduct = state => getProducts(state).filter(product => product.Top === true);
export const getNewProducts = state => getProducts(state).filter(product => product.new);